# blockchain-developer-bootcamp-final-project
 Blockchain Dev Bootcamp Final Project

Project Name: Grant Delivery Workflow

Summary: This project will allow a Contracting Officer (CO) to award a "grant" to a Company. The Company Point of Contact (CPOC) can accept the grant award and submit a monthly report during the period of the grant. The CO reviews the monthly report, and authorizes that month's payment for the work performed. 

FrontEnd Host:

Steps:

1. CO awards grant with:
	• Company Name
	• Title and Description of contract
	• Total funding
	• Number of Months
	• CLINs and Quantity

2. CO funds smart contract with award value.

3. Company POC receives award notice.

4. CPOC counter signs the contract award to accept.

5. CO countersigns to fully execute the contract.

6. CPOC delivers a MS Word Document. (via IPFS?)

7. CO reviews MS Word doc for acceptance.

8. Upon acceptance, (Total funding/number of months) funding is released to Company wallet address from smart contract.

Additional ideas:
* Independently, an auditor can go to a web page to verify all contract info and TXs. 
* Make the Grant Award an NFT? Or create a NFT at grant completion?